# Expenser is a microservice project which consist of several loosely coupled microservices
## Now it merges movie and book rental services into one system

## Services
- UserService - user management 
- MovieService - movies rental
- BookService - book rental
- ApiGatetewayService - external API with authorization and authentication
- EurekaServer - services discovering
