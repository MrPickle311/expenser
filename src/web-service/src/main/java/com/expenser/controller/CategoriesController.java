package com.expenser.controller;

import com.expenser.openapi.api.CategoriesApi;
import com.expenser.openapi.model.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class CategoriesController implements CategoriesApi {
    @Override
    public ResponseEntity<CategoryDto> addCategory(CreateCategoryDto createCategoryDto) {
        return null;
    }

    @Override
    public ResponseEntity<SubcategoryDto> addSubcategory(String categoryId, CreateSubcategoryDto createSubcategoryDto) {
        return null;
    }

    @Override
    public ResponseEntity<CategoryDto> deleteCategory(String categoryId) {
        return null;
    }

    @Override
    public ResponseEntity<SubcategoryDto> deleteSubcategory(String categoryId, String subcategoryId) {
        return null;
    }

    @Override
    public ResponseEntity<List<CategoryDto>> getCategories() {
        return null;
    }

    @Override
    public ResponseEntity<CategoryDto> getCategory(String categoryId) {
        return null;
    }

    @Override
    public ResponseEntity<List<SubcategoryDto>> getSubcategories(String categoryId) {
        return null;
    }

    @Override
    public ResponseEntity<SubcategoryDto> getSubcategory(String categoryId, String subcategoryId) {
        return null;
    }

    @Override
    public ResponseEntity<CategoryDto> modifyCategory(String categoryId, PatchCategoryDto patchCategoryDto) {
        return null;
    }

    @Override
    public ResponseEntity<SubcategoryDto> modifySubcategory(String categoryId, String subcategoryId, PatchCategoryDto patchCategoryDto) {
        return null;
    }
}
