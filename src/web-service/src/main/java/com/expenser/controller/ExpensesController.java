package com.expenser.controller;

import com.expenser.openapi.api.ExpensesApi;
import com.expenser.openapi.model.ExpenseCreateDto;
import com.expenser.openapi.model.ExpenseDto;
import com.expenser.openapi.model.PatchExpenseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ExpensesController implements ExpensesApi {
    @Override
    public ResponseEntity<List<ExpenseDto>> addExpense(ExpenseCreateDto expenseCreateDto) {
        return null;
    }

    @Override
    public ResponseEntity<List<ExpenseDto>> deleteExpense(Integer id) {
        return null;
    }

    @Override
    public ResponseEntity<Void> getExpense(Integer id) {
        return null;
    }

    @Override
    public ResponseEntity<ExpenseDto> getExpenses() {
        return null;
    }

    @Override
    public ResponseEntity<List<ExpenseDto>> modifyExpense(Integer id, PatchExpenseDto patchExpenseDto) {
        return null;
    }
}