package com.movies.model.converter;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UnixZonedTimestampConverterTest {

    private static final ZoneId SYSTEM_UTC_ZONE = Clock.systemDefaultZone().getZone();
    private final static long UNIX_TIMESTAMP = ZonedDateTime.now().toEpochSecond();
    private final static ZonedDateTime ZONED_DATE_TIME = ZonedDateTime.now();
    public static final int EXPECTED_SECOND = ZONED_DATE_TIME.getSecond();
    public static final int EXPECTED_MINUTE = ZONED_DATE_TIME.getMinute();
    public static final int EXPECTED_HOUR = ZONED_DATE_TIME.getHour();
    public static final int EXPECTED_DAY = ZONED_DATE_TIME.getDayOfMonth();
    public static final int EXPECTED_MONTH = ZONED_DATE_TIME.getMonthValue();
    public static final int EXPECTED_YEAR = ZONED_DATE_TIME.getYear();
    private final UnixZonedTimestampConverter unixZonedTimestampConverter = new UnixZonedTimestampConverter();

    @Test
    void convertLongToZonedTimestamp() {
        ZonedDateTime zonedDateTime = unixZonedTimestampConverter.convertToDatabaseColumn(UNIX_TIMESTAMP);
        assertEquals(EXPECTED_SECOND,zonedDateTime.getSecond());
        assertEquals(EXPECTED_MINUTE, zonedDateTime.getMinute());
        assertEquals(EXPECTED_HOUR, zonedDateTime.getHour());
        assertEquals(EXPECTED_DAY,zonedDateTime.getDayOfMonth());
        assertEquals(EXPECTED_MONTH,zonedDateTime.getMonthValue());
        assertEquals(EXPECTED_YEAR,zonedDateTime.getYear());
    }

    @Test
    void convertToEntityAttribute() {
        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(Instant.ofEpochSecond(UNIX_TIMESTAMP), SYSTEM_UTC_ZONE);
        long converterTimestamp = unixZonedTimestampConverter.convertToEntityAttribute(zonedDateTime);
        assertEquals(UNIX_TIMESTAMP, converterTimestamp);
    }
}