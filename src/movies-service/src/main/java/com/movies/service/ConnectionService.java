package com.movies.service;

import com.movies.model.Country;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;
import java.util.Properties;

@Service
@RequiredArgsConstructor
public class ConnectionService {

    private final JdbcTemplate jdbcTemplate;
    @EventListener(ApplicationReadyEvent.class)
    private void connect() throws SQLException {
        List<Country> countryList = jdbcTemplate.query("SELECT * FROM movies.country", BeanPropertyRowMapper.newInstance(Country.class));
        countryList.forEach(System.out::println);
    }
}
