package com.movies.model;

import com.movies.model.converter.UnixZonedTimestampConverter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "movie", schema = "movies")
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    @NotNull
    private int movieId;

    @Column(name = "title")
    private String title;

    @Column(name = "budget")
    private Integer budget;

    @Column(name = "homepage")
    private String homepage;

    @Column(name = "popularity", precision = 12, scale = 6)
    private Double popularity;

    @Column(name = "release_date")
    @Convert(converter = UnixZonedTimestampConverter.class)
    private Long releaseDate;

    @Column(name = "revenue")
    private Long revenue;

    @Column(name = "runtime")
    private Integer runtime;

    @Column(name = "movie_status")
    @Size(max = 50)
    private String movieStatus;

    @Column(name = "tagline")
    private String tagLine;

    @Column(name = "vote_average", precision = 4, scale = 2)
    private Double voteAverage;

    @Column(name = "vote_count")
    private Double voteCount;

    @ManyToMany
    @JoinTable(
            name = "production_country",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "country_id")
    )
    private Set<Country> productionCountry;

    @ManyToMany
    @JoinTable(
            name = "movie_keywords",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "keyword_id")
    )
    private Set<Keyword> keywords;

    @ManyToMany
    @JoinTable(
            name = "movie_company",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id")
    )
    private Set<ProductionCompany> productionCompanies;

    @ManyToMany
    @JoinTable(
            name = "movie_genres",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id")
    )
    private Set<Genre> genres;

    @OneToMany(mappedBy = "movie")
    private Set<MovieLanguage> movieLanguages;

    @OneToMany(mappedBy = "movie")
    private Set<MovieCrew> movieCrews;

    @OneToMany(mappedBy = "movie")
    private Set<MovieCast> movieCasts;
}
