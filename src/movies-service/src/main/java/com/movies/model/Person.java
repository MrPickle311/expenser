package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "person",schema = "movies")
@Getter
@Setter
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id")
    @NotNull
    private int personId;

    @Column(name = "person_name")
    private String personName;

    @OneToMany(mappedBy = "person")
    private Set<MovieCast> movieCasts;
}
