package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "production_company", schema = "movies")
@Getter
@Setter
public class ProductionCompany {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "company_id")
    @NotNull
    private int companyId;

    @Column(name = "company_name")
    private String companyName;

    @ManyToMany(mappedBy = "productionCompanies")
    private Set<Movie> movies;
}
