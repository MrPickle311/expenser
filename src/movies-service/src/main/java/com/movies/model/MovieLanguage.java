package com.movies.model;

import com.movies.model.key.MovieLanguagesKey;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "movie_language", schema = "movies")
@Getter
@Setter
public class MovieLanguage {

    @EmbeddedId
    private MovieLanguagesKey movieLanguagesKey;

    @ManyToOne
    @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @ManyToOne
    @MapsId("languageId")
    @JoinColumn(name = "language_id")
    private Language language;

    @ManyToOne
    @MapsId("languageRoleId")
    @JoinColumn(name = "language_role_id")
    private LanguageRole languageRole;
}
