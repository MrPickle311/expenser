package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@Table(name = "keyword", schema = "movies")
@Getter
@Setter
public class Keyword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "keyword_id")
    @NotNull
    private int keywordId;

    @Column(name = "keyword_name")
    private String keywordName;

    @ManyToMany(mappedBy = "keywords")
    private Set<Movie> movies;
}
