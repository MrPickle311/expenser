package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "country", schema = "movies")
@Getter
@Setter
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    @NotNull
    private int countryId;

    @Column(name = "country_iso_code")
    @Size(max = 10)
    private String countryLsoCode;
    @Column(name = "country_name")
    private String countryName;

    @ManyToMany(mappedBy = "productionCountry")
    private Set<Movie> movies;
}
