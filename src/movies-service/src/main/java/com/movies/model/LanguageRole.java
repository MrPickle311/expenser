package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "language_role", schema = "movies")
@Getter
@Setter
public class LanguageRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    @NotNull
    private int languageRoleId;

    @Column(name = "language_role")
    @Size(max = 20)
    private String languageRoleName;

    @OneToMany(mappedBy = "languageRole")
    private Set<MovieLanguage> movieLanguages;
}
