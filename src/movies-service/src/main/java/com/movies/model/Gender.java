package com.movies.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "gender", schema = "movies")
@Getter
@Setter
public class Gender {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "gender_id")
    @NotNull
    private Long genderId;

    @Column(name = "gender")
    @Size(max = 20)
    private String genderName;

    @OneToMany(mappedBy = "gender")
    private Set<MovieCast> movieCasts;
}
