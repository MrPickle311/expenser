package com.movies.model;

import com.movies.model.key.MovieCrewKey;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "movie_crew", schema = "movies")
@Getter
@Setter
public class MovieCrew {
    @EmbeddedId
    private MovieCrewKey movieCrewKey;

    @Column(name = "job")
    private String job;

    @ManyToOne
    @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @ManyToOne
    @MapsId("personId")
    @JoinColumn(name = "person_id")
    private Person person;

    @ManyToOne
    @MapsId("departmentId")
    @JoinColumn(name = "department_id")
    private Department department;
}