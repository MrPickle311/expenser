package com.movies.model.key;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class MovieCrewKey implements Serializable {
    @Column(name = "movie_id")
    @NotNull
    private Integer movieId;

    @Column(name = "person_id")
    @NotNull
    private Integer personId;

    @Column(name = "department_id")
    @NotNull
    private Integer departmentId;
}
