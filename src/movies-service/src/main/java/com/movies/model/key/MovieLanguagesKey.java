package com.movies.model.key;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class MovieLanguagesKey implements Serializable {
    @Column(name = "movie_id")
    @NotNull
    private Integer movieId;

    @Column(name = "language_id")
    @NotNull
    private Integer languageId;

    @Column(name = "language_role_id")
    @NotNull
    private Integer languageRoleId;


}
