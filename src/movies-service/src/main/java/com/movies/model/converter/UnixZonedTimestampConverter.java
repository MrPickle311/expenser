package com.movies.model.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.*;

@Converter
public class UnixZonedTimestampConverter implements AttributeConverter<Long, ZonedDateTime> {
    private static final ZoneId SYSTEM_UTC_ZONE = Clock.systemDefaultZone().getZone();

    @Override
    public ZonedDateTime convertToDatabaseColumn(Long timestamp) {
        return ZonedDateTime.ofInstant(Instant.ofEpochSecond(timestamp), SYSTEM_UTC_ZONE);
    }

    @Override
    public Long convertToEntityAttribute(ZonedDateTime zonedDateTime) {
        return zonedDateTime.toEpochSecond();
    }
}