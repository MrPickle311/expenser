package com.movies.model;

import com.movies.model.key.MovieCastKey;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "movie_cast", schema = "movies")
@Getter
@Setter
public class MovieCast {
    @EmbeddedId
    private MovieCastKey movieCastKey;

    @ManyToOne
    @MapsId("movieId")
    @JoinColumn(name = "movie_id")
    private Movie movie;

    @ManyToOne
    @MapsId("personId")
    @JoinColumn(name = "person_id")
    private Person person;

    @ManyToOne
    @MapsId("genderId")
    @JoinColumn(name = "gender_id")
    private Gender gender;
}
