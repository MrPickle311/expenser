package com.movies.model.key;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
public class MovieCastKey implements Serializable {

    @Column(name = "movie_id")
    @NotNull
    private Integer movieId;

    @Column(name = "person_id")
    @NotNull
    private Integer personId;

    @Column(name = "gender_id")
    @NotNull
    private Integer genderId;
}