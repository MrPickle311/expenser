package com.movies.controller;

import com.expenser.openapi.api.GendersApi;
import com.expenser.openapi.model.Company;
import com.expenser.openapi.model.ExistingCompany;
import com.expenser.openapi.model.ExistingGender;
import com.expenser.openapi.model.Gender;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class GendersController implements GendersApi {
    @Override
    public ResponseEntity<List<ExistingGender>> getGenders(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCompany> insertCompany(Company company) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGender> removeGender() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGender> updateGender(Gender gender) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGender> upsertGender(Gender gender) {
        return null;
    }
}
