package com.movies.controller;

import com.expenser.openapi.api.MoviesApi;
import com.expenser.openapi.model.ExistingMovie;
import com.expenser.openapi.model.Movie;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class MoviesController implements MoviesApi {
    @Override
    public ResponseEntity<List<ExistingMovie>> getMovies(String filter, String inclusions, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingMovie> insertMovie(Movie movie) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingMovie> removeMovie() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingMovie> updateMovie(Movie movie) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingMovie> upsertMovie(Movie movie) {
        return null;
    }
}
