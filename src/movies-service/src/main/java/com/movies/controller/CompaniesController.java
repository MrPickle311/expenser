package com.movies.controller;

import com.expenser.openapi.api.CompaniesApi;
import com.expenser.openapi.model.Company;
import com.expenser.openapi.model.ExistingCompany;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class CompaniesController implements CompaniesApi {
    @Override
    public ResponseEntity<List<ExistingCompany>> getCompanies(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCompany> insertCompany(Company company) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCompany> removeCountry() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCompany> updateCompany(Company company) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCompany> upsertCompany(Company company) {
        return null;
    }
}
