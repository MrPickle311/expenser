package com.movies.controller;

import com.expenser.openapi.api.LanguagesApi;
import com.expenser.openapi.model.ExistingLanguage;
import com.expenser.openapi.model.Language;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class LanguagesController implements LanguagesApi {
    @Override
    public ResponseEntity<List<ExistingLanguage>> getLanguages(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguage> insertLanguage(Language language) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguage> removeLanguage() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguage> updateLanguage(Language language) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguage> upsertLanguage(Language language) {
        return null;
    }
}
