package com.movies.controller;

import com.expenser.openapi.api.LanguageRolesApi;
import com.expenser.openapi.model.ExistingLanguageRole;
import com.expenser.openapi.model.LanguageRole;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class LanguageRolesController implements LanguageRolesApi {
    @Override
    public ResponseEntity<List<ExistingLanguageRole>> getLanguagesRoles(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguageRole> insertLanguageRole(LanguageRole languageRole) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguageRole> removeLanguageRole() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguageRole> updateLanguageRole(LanguageRole languageRole) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingLanguageRole> upsertLanguageRole(LanguageRole languageRole) {
        return null;
    }
}
