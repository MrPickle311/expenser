package com.movies.controller;

import com.expenser.openapi.api.KeywordsApi;
import com.expenser.openapi.model.ExistingCountry;
import com.expenser.openapi.model.ExistingKeyword;
import com.expenser.openapi.model.Keyword;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class KeywordsController implements KeywordsApi {
    @Override
    public ResponseEntity<List<ExistingKeyword>> getKeywords(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingKeyword> insertKeyword(Keyword keyword) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCountry> removeCountry() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingKeyword> updateKeyword(Keyword keyword) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingKeyword> upsertKeyword(Keyword keyword) {
        return null;
    }
}
