package com.movies.controller;

import com.expenser.openapi.api.CountriesApi;
import com.expenser.openapi.model.Country;
import com.expenser.openapi.model.ExistingCountry;
import com.expenser.openapi.model.ExistingMovie;
import com.expenser.openapi.model.Movie;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class CountriesController implements CountriesApi {
    @Override
    public ResponseEntity<List<ExistingMovie>> getCountries(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCountry> insertCountry(Country country) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCountry> removeCountry() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingCountry> updateCountry(Country country) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingMovie> upsertCountry(Movie movie) {
        return null;
    }
}
