package com.movies.controller;

import com.expenser.openapi.api.PeopleApi;
import com.expenser.openapi.model.ExistingPerson;
import com.expenser.openapi.model.Person;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class PeopleController implements PeopleApi {
    @Override
    public ResponseEntity<List<ExistingPerson>> getPeople(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingPerson> insertPerson(Person person) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingPerson> removePerson() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingPerson> updatePerson(Person person) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingPerson> upsertPerson(Person person) {
        return null;
    }
}
