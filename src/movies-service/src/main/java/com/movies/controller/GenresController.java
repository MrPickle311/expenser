package com.movies.controller;

import com.expenser.openapi.api.GenresApi;
import com.expenser.openapi.model.ExistingGenre;
import com.expenser.openapi.model.Genre;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class GenresController implements GenresApi {
    @Override
    public ResponseEntity<List<ExistingGenre>> getGenres(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGenre> insertGenre(Genre genre) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGenre> removeGenre() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGenre> updateGenre(Genre genre) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingGenre> upsertGenre(Genre genre) {
        return null;
    }
}
