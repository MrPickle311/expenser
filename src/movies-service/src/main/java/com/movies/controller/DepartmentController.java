package com.movies.controller;

import com.expenser.openapi.api.DepartmentApi;
import com.expenser.openapi.model.Department;
import com.expenser.openapi.model.ExistingDepartment;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class DepartmentController implements DepartmentApi {
    @Override
    public ResponseEntity<List<ExistingDepartment>> getDepartments(String filter, Integer page, Integer size) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingDepartment> insertDepartment(Department department) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingDepartment> removeDepartment() {
        return null;
    }

    @Override
    public ResponseEntity<ExistingDepartment> updateDepartment(Department department) {
        return null;
    }

    @Override
    public ResponseEntity<ExistingDepartment> upsertDepartment(Department department) {
        return null;
    }
}
