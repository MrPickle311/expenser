package com.movies.repository;

import com.movies.model.LanguageRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRoleRepository extends JpaRepository<LanguageRole,Integer> {
}
